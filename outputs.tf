output "id" {
    description = "Nutanix UUID of the VM"
    value       = "${nutanix_virtual_machine.hello.id}"
}