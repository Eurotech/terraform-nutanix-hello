variable "cluster" {
    type        = "string"
    description = "UUID of the deployment cluster"
}

variable "network" {
    type        = "string"
    description = "UUID of the deployment network"
}

variable "message" {
    type        = "string"
    default     = "Hello, Nutanix!"
    description = "A welcome message to display in the web GUI"
}